package com.example.mvvm_changing_bg;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class MainViewModel implements IMainViewModel {

    final BehaviorSubject<String> colorPublishSubject = BehaviorSubject.create();

    @Override
    public Observable<String> getColorSubject() {
        return colorPublishSubject;
    }

    @Override
    public void onColorChanged (String color) {
        colorPublishSubject.onNext(color);
    }
}

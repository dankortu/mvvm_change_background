package com.example.mvvm_changing_bg;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mvvm_changing_bg.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    IMainViewModel mainViewModel = new MainViewModel();
    Disposable colorDisposable;
    boolean isPressed = true;
    List<MyColor> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Spinner spinner = binding.spinner;
        list.addAll(Arrays.asList(new MyColor("Red", "#eb4034"), new MyColor("White", "#ffffff"), new MyColor("Green", "#82c977")));
        ArrayAdapter<MyColor> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                mainViewModel.onColorChanged(list.get(selectedItemPosition).value);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final Button button = binding.subscribeBtn;
        button.setOnClickListener(this::subscribeOnColor);

        colorDisposable = mainViewModel.getColorSubject().subscribe(this::handleColor);
    }

    private void subscribeOnColor(View view) {
        if (!isPressed) {
            colorDisposable.dispose();
            colorDisposable = mainViewModel.getColorSubject().subscribe(this::handleColorForBtn);
            isPressed = true;
            binding.subscribeBtn.setText("ОТПИСКА");
        } else {
            colorDisposable.dispose();
            colorDisposable = mainViewModel.getColorSubject().subscribe(this::handleColor);
            isPressed = false;
            binding.subscribeBtn.setText("ПОДПИСКА");
        }


    }

    private void handleColorForBtn(String s) {
        binding.subscribeBtn.setBackgroundColor(Color.parseColor(s));
    }

    private void handleColor(String s) {
        binding.main.setBackgroundColor(Color.parseColor(s));
    }


    protected void onDestroy() {
        if (colorDisposable != null && !colorDisposable.isDisposed()) {
            colorDisposable.dispose();
        }

        super.onDestroy();
    }
}
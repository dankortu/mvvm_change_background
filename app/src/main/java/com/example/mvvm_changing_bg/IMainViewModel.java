package com.example.mvvm_changing_bg;


import io.reactivex.Observable;
import io.reactivex.Single;

public interface IMainViewModel {
    Observable<String> getColorSubject();
    void onColorChanged(String color);
}

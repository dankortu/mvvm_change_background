package com.example.mvvm_changing_bg;

import androidx.annotation.NonNull;

public class MyColor {
    public String key;
    public String value;

    public MyColor(String key, String value) {
        this.key = key;
        this.value= value;
    }

    @NonNull
    @Override
    public String toString() {
        return key;
    }
}
